<?php
/**
 * Created by PhpStorm.
 * User: laravel
 * Date: 1/27/18
 * Time: 4:58 PM
 */

namespace App\Presenters;


use App\Presenters\Contract\Presenter;

class CategoryPresenter extends Presenter
{
    public function category_parent_title()
    {

        return isset($this->entity->parent)? $this->entity->parent->category_title : '--';
    }
}