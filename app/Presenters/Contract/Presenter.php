<?php
/**
 * Created by PhpStorm.
 * User: laravel
 * Date: 1/23/18
 * Time: 6:11 PM
 */

namespace App\Presenters\Contract;


abstract class Presenter
{
    protected $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }
        return $this->entity->{$property};


    }

}