<?php

namespace App\Presenters;


use App\Presenters\Contract\Presenter;

class ProductPresenter extends Presenter
{
    public function product_price_in_hezar_toman()
    {
        return number_format($this->product_price/1000).' هزار تومان';
    }
}