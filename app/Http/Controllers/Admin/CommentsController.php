<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Comment\CommentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends BaseController
{
    public function __construct()
    {
        parent::__construct(CommentRepository::class);
    }

    public function index(Request $request)
    {

        $comments = $this->repository->all();

        if ($request->has('pid') && intval($request->input('pid')) > 0){

            $comments=$this->repository->commentByModel($request->input('pid'));
        }

        $title = 'کامنت';
        //return $comments;
        return view('admin.comment.index', compact('title', 'comments'));

        // return 123;
    }
}
