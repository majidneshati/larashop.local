<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;

class ProductsController extends BaseController
{
    public function __construct()
    {
        parent::__construct(ProductRepository::class);
//        $this->repository = new ProductRepository();
    }

    public function index(Request $request)
    {


        $title = "همه محصولات";

        $products = $this->repository->all();

        return view('admin.product.index', compact('title', 'products'));

    }

    public function edit()
    {
        $title = "ایجاد محصول جدید";
        $categories=(new CategoryRepository())->all()->groupBy('category_parent_id');
        return view('admin.product.create', compact('title','categories'));
    }

    public function store(Request $request)
    {

        $request->validate([
            "product_title" => "required",
            "product_price" => "required|integer",
            "product_stock" => "required|integer",
            "product_type" => "required|integer",
            "product_discount" => "integer",
            "product_coupon_count" => "integer",
            "product_description" => "required",
            "product_status" => "required|integer",
            "product_visible" => "required"
        ], [
            "product_stock.required" => "موجودی وارد نشده ",
        ]);

         //dd($request->input('product_categories'));

        // $this->repository->create($request->all());

        $newProduct= $this->repository->create([
            "product_code" => 0,
            "product_title" => $request->input('product_title'),
            "product_slug" => '',
            "product_price" => $request->input('product_price'),
            "product_stock" => $request->input('product_stock'),
            "product_type" => $request->input('product_type'),
            "product_discount" => $request->input('product_discount'),
            "product_coupon_count" => $request->input('product_coupon_count'),
            "product_description" => $request->input('product_description'),
            "product_status" => $request->input('product_status'),
            "product_visible" => $request->exists('product_visible') ? 1 : 0,
        ]);

        if($newProduct && is_a($newProduct,Product::class)){

            $newProduct->categories()->attach( $request->input('product_categories') );

            return redirect()->back()->with('status','محصول با موفقیت ذخیره شد');
        }

    }

}
