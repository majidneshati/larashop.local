<?php

namespace App\Repositories\Category;


use App\Models\Category;
use App\Repositories\Contract\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();

        $this->model = Category::class;
    }

}