<?php

namespace App\Models;

use App\Presenters\Currency;
use App\Traits\HasComment;
use App\Traits\HasTag;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
//    protected $table = 'custom_product_table_name';
    use HasComment,HasTag;

    protected $primaryKey = 'product_id';

    protected $guarded = ['product_id']; //black list

    //protected $fillable=[]; //white list

    public function getProductPriceAttribute()
    {
        return Currency::toman($this->attributes['product_price']);
    }

    public function setProductSlugAttribute()
    {
        $this->attributes['product_slug'] = preg_replace('/\s+/', '-', $this->attributes['product_title']);
    }


    public function categories()
    {
        return $this->belongsToMany(Category::class ,'product_categories','product_id','category_id');
    }



}
