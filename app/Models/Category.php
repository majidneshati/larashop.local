<?php

namespace App\Models;

use App\Presenters\CategoryPresenter;
use App\Presenters\Contract\Presentable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Presentable;

    protected $primaryKey = 'category_id';

    protected $guarded = ["category_id"];

    public $timestamps = false;

    protected $presenter = CategoryPresenter::class;

    //const CREATED_AT = '';
    //const UPDATED_AT = '';


    public function parent()
    {

        return $this->belongsTo(Category::class,'category_parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class ,'product_categories','category_id','product_id');
    }
}
