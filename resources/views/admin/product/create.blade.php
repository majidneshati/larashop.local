@extends('layouts.admin')

@section('content')

    <div class="box box-primary container">
        <div class="box-header with-border">
            <h3 class="box-title">{{$title}}</h3>
        </div>

        @include('partials.admin.errors')

        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form method="post" action="{{ route('admin.products.store') }}">
            {{ csrf_field()  }}
            <div class="box-body">

                <div class="form-group">
                    <label>عنوان محصول :</label>
                    <input
                            type="text"
                            name="product_title"
                            class="form-control">

                    @if ($errors->any('product_title'))
                        <div class="alert alert-danger"> عنوان محصول وارد نشده</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>قیمت محصول :</label>
                    <input
                            type="text"
                            name="product_price"
                            class="form-control">
                </div>
                <div class="form-group">
                    <label>موجودی محصول :</label>
                    <input
                            type="text"
                            name="product_stock"
                            class="form-control">
                </div>
                <div class="form-group">
                    <label>تخفیف محصول :</label>
                    <input
                            type="text"
                            name="product_discount"
                            class="form-control">
                </div>
                <div class="form-group">
                    <label>نوع محصول :</label>
                    <input type="radio" name="product_type" value="1">
                    فیزیکی
                    <input type="radio" name="product_type" value="2">
                    مجازی

                </div>
                <div class="form-group">
                    <label>تعداد کوپن محصول :</label>
                    <input
                            type="text"
                            name="product_coupon_count"
                            class="form-control">
                </div>
                <div class="form-group">
                    <label>توضیحات محصول :</label>
                    <textarea name="product_description" class="form-control" id="" cols="30" rows="10"></textarea>

                </div>
                <div class="form-group">
                    <label>دسته بندی های محصول :</label>

                        @include('admin.category.category_tree',['collection' => $categories[0] ])
                </div>
                <div class="form-group">
                    <label>وضعیت محصول :</label>
                    <select name="product_status" id="product_status">
                        <option value="1">عادی</option>
                        <option value="2">پیش فروش</option>
                    </select>
                </div>


                <div class="checkbox">
                    <label for="product_visible">
                        <input type="checkbox"id="product_visible" name="product_visible">  نمایش محصول
                    </label>
                </div>

                <button type="submit" class="btn btn-fill btn-info">ذخیره اطلاعات</button>

            </div>
        </form>
    </div>



















    <div class="row">
        <div class="col-md-12">

            <div class="card">

                <div class="content">

                </div> <!-- end card -->

            </div>
        </div>
@endsection