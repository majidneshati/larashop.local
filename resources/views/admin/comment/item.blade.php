<tr>
    <td>{{ $comment->comment_id  }}</td>
    <td>{{ $comment->user->name  }}</td>
    <td>{{ $comment->comment_approved  }}</td>
    <td>{{ $comment->created_at  }}</td>
    <td class="text-center" width="120">
            <a href="#" class="btn btn-default bg-orange"><i class="fa fa-edit"></i></a>
            <a href="#" class="btn btn-danger"><i class="fa fa-remove"></i></a>
    </td>