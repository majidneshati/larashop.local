<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {


    Route::get('/', 'DashboardController@index')->name('admin.dashboard');

    //product
    Route::get('products', 'ProductsController@index')->name('admin.products');
    Route::get('products/create', 'ProductsController@edit')->name('admin.products.create');
    Route::post('products/create', 'ProductsController@store')->name('admin.products.store');

    //category
    Route::get('products/category/', 'CategoriesController@index')->name('admin.category');
    Route::get('products/category/create', 'CategoriesController@create')->name('admin.category.create');
    Route::post('products/category/create', 'CategoriesController@store')->name('admin.category.store');

    //comments
    Route::get('products/comments', 'CommentsController@index')->name('admin.comments');
});
